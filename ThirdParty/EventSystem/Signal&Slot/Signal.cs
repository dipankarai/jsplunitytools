﻿using System;
using System.Collections.Generic;

namespace Signals
{
    public class Signal
    {
        protected List<Slot> slots = new List<Slot>();

        private List<Slot> slotsPooled = new List<Slot>();
        private List<Slot> slotsRemoved = new List<Slot>();
        private int numDispatches = 0;

        public static implicit operator bool(Signal signal)
        {
            return signal != null;
        }

        public Signal()
        {

        }

        /// <summary>
        /// Cleans up the Signal by removing and disposing all listeners,
        /// and unpooling allocated Slots.
        /// </summary>
        public void Dispose()
        {
            RemoveAll();
            while (slotsPooled.Count > 0)
            {
                Slot slot = slotsPooled[slotsPooled.Count - 1];
                slotsPooled.RemoveAt(slotsPooled.Count - 1);
                slot.Dispose();
            }
        }

        /// <summary>
        /// Calls Dispose only if there are no listeners.
        /// </summary>
        public bool DisposeIfEmpty()
        {
            if (slots.Count <= 0)
            {
                Dispose();
                return true;
            }
            return false;
        }

        protected bool DispatchStart()
        {
            if (slots.Count > 0)
            {
                ++numDispatches;
                return true;
            }
            return false;
        }

        protected bool DispatchStop()
        {
            --numDispatches;
            if (numDispatches == 0)
            {
                while (slotsRemoved.Count > 0)
                {
                    Slot slot = slotsRemoved[slotsRemoved.Count - 1];
                    slotsRemoved.RemoveAt(slotsRemoved.Count - 1);
                    DisposeSlot(slot);
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// The number of concurrent dispatches. During a dispatch, it's possible that external
        /// code could require another dispatch on the same Signal.
        /// </summary>
        public int NumDispatches
        {
            get
            {
                return numDispatches;
            }
        }

        /// <summary>
        /// The number of Slots/listeners attached to this Signal.
        /// </summary>
        public int NumSlots
        {
            get
            {
                return slots.Count;
            }
        }

        /// <summary>
        /// Returns a copy of the Slots being processed by this Signal in order of
        /// how they're prioritized.
        /// </summary>
        public List<Slot> Slots
        {
            get
            {
                return new List<Slot>(slots);
            }
        }

        private void DisposeSlot(Slot slot)
        {
            slot.Signal = null;
            slot.Dispose();
            slotsPooled.Add(slot);
        }

        public Slot Get(Delegate listener)
        {
            if (listener != null)
            {
                foreach (Slot slot in slots)
                {
                    if (slot.Listener == listener)
                    {
                        return slot;
                    }
                }
            }
            return null;
        }

        public Slot GetAt(int index)
        {
            if (index < 0)
                return null;
            if (index > slots.Count - 1)
                return null;
            return slots[index];
        }

        public int GetIndex(Delegate listener)
        {
            if (listener != null)
            {
                for (int index = slots.Count - 1; index > -1; --index)
                {
                    if (slots[index].Listener == listener)
                    {
                        return index;
                    }
                }
            }
            return -1;
        }

        public Slot Add(Delegate listener, int priority = 0, bool isOnce = false)
        {
            if (listener != null)
            {
                Slot slot = Get(listener);
                if (slot == null)
                {
                    if (slotsPooled.Count > 0)
                    {
                        slot = slots[slotsPooled.Count - 1];
                        slotsPooled.RemoveAt(slotsPooled.Count - 1);
                    }
                    else
                    {
                        slot = CreateSlot();
                    }

                    slot.Listener = listener;
                    slot.Priority = priority;
                    slot.IsOnce = isOnce;
                    slot.Signal = this;

                    PriorityChanged(slot, slot.Priority, 0);

                    return slot;
                }
            }
            return null;
        }

        virtual protected Slot CreateSlot()
        {
            return null;
        }

        internal void PriorityChanged(Slot slot, int current, int previous)
        {
            slots.Remove(slot);

            for (int index = slots.Count; index > 0; --index)
            {
                if (slots[index - 1].Priority <= slot.Priority)
                {
                    slots.Insert(index, slot);
                    return;
                }
            }

            slots.Insert(0, slot);
        }

        public bool Remove(Delegate listener)
        {
            if (listener != null)
            {
                for (int index = slots.Count - 1; index > -1; --index)
                {
                    if (slots[index].Listener == listener)
                    {
                        return RemoveAt(index);
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Removes the Slot/listener at the given index.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool RemoveAt(int index)
        {
            if (index > 0 && index < slots.Count)
            {
                Slot slot = slots[index];
                slots.RemoveAt(index);

                if (numDispatches > 0)
                {
                    slotsRemoved.Add(slot);
                }
                else
                {
                    DisposeSlot(slot);
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Removes all Slots/listeners.
        /// </summary>
        public void RemoveAll()
        {
            while (slots.Count > 0)
            {
                RemoveAt(slots.Count - 1);
            }
        }
    }

    public sealed class Signal<T1>:Signal
    {
        public Signal()
        {

        }

        public void Dispatch(T1 item1)
        {
            if (DispatchStart())
            {
                foreach (Slot<T1> slot in Slots)
                {
                    if (slot.IsOnce)
                    {
                        Remove(slot.Listener);
                    }

                    try
                    {
                        slot.Listener.Invoke(item1);
                    }
                    catch
                    {
                        //We remove the Slot so the Error doesn't inevitably happen again.
                        Remove(slot.Listener);
                    }
                }

                DispatchStop();
            }
        }

        public new List<Slot<T1>> Slots
        {
            get
            {
                List<Slot<T1>> slots = new List<Slot<T1>>();
                foreach (Slot<T1> slot in this.slots)
                {
                    slots.Add(slot);
                }
                return slots;
            }
        }

        public Slot<T1> Get(Action<T1> listener)
        {
            return (Slot<T1>)base.Get(listener);
        }

        public new Slot<T1> GetAt(int index)
        {
            return (Slot<T1>)base.GetAt(index);
        }

        public int GetIndex(Action<T1> listener)
        {
            return base.GetIndex(listener);
        }

        public Slot<T1> Add(Action<T1> listener)
        {
            return Add(listener, 0, false);
        }

        public Slot<T1> Add(Action<T1> listener, int priority)
        {
            return Add(listener, priority, false);
        }

        public Slot<T1> AddOnce(Action<T1> listener, int priority)
        {
            return Add(listener, priority, true);
        }

        public Slot<T1> Add(Action<T1> listener, int priority, bool isOnce)
        {
            return (Slot<T1>)base.Add(listener, priority, isOnce);
        }

        public bool Remove(Action<T1> listener)
        {
            return base.Remove(listener);
        }

        override protected Slot CreateSlot()
        {
            return new Slot<T1>();
        }
    }

    public sealed class Signal<T1, T2>:Signal
    {
        public Signal()
        {

        }

        public void Dispatch(T1 item1, T2 item2)
        {
            if (DispatchStart())
            {
                foreach (Slot<T1, T2> slot in Slots)
                {
                    if (slot.IsOnce)
                    {
                        Remove(slot.Listener);
                    }

                    try
                    {
                        slot.Listener.Invoke(item1, item2);
                    }
                    catch
                    {
                        //We remove the Slot so the Error doesn't inevitably happen again.
                        Remove(slot.Listener);
                    }
                }

                DispatchStop();
            }
        }

        public new List<Slot<T1, T2>> Slots
        {
            get
            {
                List<Slot<T1, T2>> slots = new List<Slot<T1, T2>>();
                foreach (Slot<T1, T2> slot in this.slots)
                {
                    slots.Add(slot);
                }
                return slots;
            }
        }

        public Slot<T1, T2> Get(Action<T1, T2> listener)
        {
            return (Slot<T1, T2>)base.Get(listener);
        }

        public new Slot<T1, T2> GetAt(int index)
        {
            return (Slot<T1, T2>)base.GetAt(index);
        }

        public int GetIndex(Action<T1, T2> listener)
        {
            return base.GetIndex(listener);
        }

        public Slot<T1, T2> Add(Action<T1, T2> listener)
        {
            return Add(listener, 0, false);
        }

        public Slot<T1, T2> Add(Action<T1, T2> listener, int priority)
        {
            return Add(listener, priority, false);
        }

        public Slot<T1, T2> AddOnce(Action<T1, T2> listener, int priority)
        {
            return Add(listener, priority, true);
        }

        public Slot<T1, T2> Add(Action<T1, T2> listener, int priority, bool isOnce)
        {
            return (Slot<T1, T2>)base.Add(listener, priority, isOnce);
        }

        public bool Remove(Action<T1, T2> listener)
        {
            return base.Remove(listener);
        }

        override protected Slot CreateSlot()
        {
            return new Slot<T1, T2>();
        }
    }

    public sealed class Signal<T1, T2, T3>:Signal
    {
        public Signal()
        {

        }

        public void Dispatch(T1 item1, T2 item2, T3 item3)
        {
            if (DispatchStart())
            {
                foreach (Slot<T1, T2, T3> slot in Slots)
                {
                    if (slot.IsOnce)
                    {
                        Remove(slot.Listener);
                    }

                    try
                    {
                        slot.Listener.Invoke(item1, item2, item3);
                    }
                    catch
                    {
                        //We remove the Slot so the Error doesn't inevitably happen again.
                        Remove(slot.Listener);
                    }
                }

                DispatchStop();
            }
        }

        public new List<Slot<T1, T2, T3>> Slots
        {
            get
            {
                List<Slot<T1, T2, T3>> slots = new List<Slot<T1, T2, T3>>();
                foreach (Slot<T1, T2, T3> slot in this.slots)
                {
                    slots.Add(slot);
                }
                return slots;
            }
        }

        public Slot<T1, T2, T3> Get(Action<T1, T2, T3> listener)
        {
            return (Slot<T1, T2, T3>)base.Get(listener);
        }

        public new Slot<T1, T2, T3> GetAt(int index)
        {
            return (Slot<T1, T2, T3>)base.GetAt(index);
        }

        public int GetIndex(Action<T1, T2, T3> listener)
        {
            return base.GetIndex(listener);
        }

        public Slot<T1, T2, T3> Add(Action<T1, T2, T3> listener)
        {
            return Add(listener, 0, false);
        }

        public Slot<T1, T2, T3> Add(Action<T1, T2, T3> listener, int priority)
        {
            return Add(listener, priority, false);
        }

        public Slot<T1, T2, T3> AddOnce(Action<T1, T2, T3> listener, int priority)
        {
            return Add(listener, priority, true);
        }

        public Slot<T1, T2, T3> Add(Action<T1, T2, T3> listener, int priority, bool isOnce)
        {
            return (Slot<T1, T2, T3>)base.Add(listener, priority, isOnce);
        }

        public bool Remove(Action<T1, T2, T3> listener)
        {
            return base.Remove(listener);
        }

        override protected Slot CreateSlot()
        {
            return new Slot<T1, T2, T3>();
        }
    }

    public sealed class Signal<T1, T2, T3, T4>:Signal
    {
        public Signal()
        {

        }

        public void Dispatch(T1 item1, T2 item2, T3 item3, T4 item4)
        {
            if (DispatchStart())
            {
                foreach (Slot<T1, T2, T3, T4> slot in Slots)
                {
                    if (slot.IsOnce)
                    {
                        Remove(slot.Listener);
                    }

                    try
                    {
                        slot.Listener.Invoke(item1, item2, item3, item4);
                    }
                    catch
                    {
                        //We remove the Slot so the Error doesn't inevitably happen again.
                        Remove(slot.Listener);
                    }
                }

                DispatchStop();
            }
        }

        public new List<Slot<T1, T2, T3, T4>> Slots
        {
            get
            {
                List<Slot<T1, T2, T3, T4>> slots = new List<Slot<T1, T2, T3, T4>>();
                foreach (Slot<T1, T2, T3, T4> slot in this.slots)
                {
                    slots.Add(slot);
                }
                return slots;
            }
        }

        public Slot<T1, T2, T3, T4> Get(Action<T1, T2, T3, T4> listener)
        {
            return (Slot<T1, T2, T3, T4>)base.Get(listener);
        }

        public new Slot<T1, T2, T3, T4> GetAt(int index)
        {
            return (Slot<T1, T2, T3, T4>)base.GetAt(index);
        }

        public int GetIndex(Action<T1, T2, T3, T4> listener)
        {
            return base.GetIndex(listener);
        }

        public Slot<T1, T2, T3, T4> Add(Action<T1, T2, T3, T4> listener)
        {
            return Add(listener, 0, false);
        }

        public Slot<T1, T2, T3, T4> Add(Action<T1, T2, T3, T4> listener, int priority)
        {
            return Add(listener, priority, false);
        }

        public Slot<T1, T2, T3, T4> AddOnce(Action<T1, T2, T3, T4> listener, int priority)
        {
            return Add(listener, priority, true);
        }

        public Slot<T1, T2, T3, T4> Add(Action<T1, T2, T3, T4> listener, int priority, bool isOnce)
        {
            return (Slot<T1, T2, T3, T4>)base.Add(listener, priority, isOnce);
        }

        public bool Remove(Action<T1, T2, T3, T4> listener)
        {
            return base.Remove(listener);
        }

        override protected Slot CreateSlot()
        {
            return new Slot<T1, T2, T3, T4>();
        }
    }
}