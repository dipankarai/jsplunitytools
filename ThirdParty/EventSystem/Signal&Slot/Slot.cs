﻿using System;

namespace Signals
{
    public class Slot
    {
        protected Signal signal;
        protected Delegate listener;
        private int priority = 0;
        private bool isOnce = false;

        public static implicit operator bool(Slot slot)
        {
            return slot != null;
        }

        internal Slot()
        {

        }

        public void Dispose()
        {
            if (signal != null)
            {
                signal.Remove(listener);
            }
            else
            {
                signal = null;
                listener = null;
                priority = 0;
                isOnce = false;
            }
        }

        public Signal Signal
        {
            get
            {
                return signal;
            }
            internal set
            {
                signal = value;
            }
        }

        public Delegate Listener
        {
            get
            {
                return listener;
            }
            internal set
            {
                listener = value;
            }
        }

        public bool IsOnce
        {
            get
            {
                return isOnce;
            }
            set
            {
                isOnce = value;
            }
        }

        public int Priority
        {
            get
            {
                return priority;
            }
            set
            {
                if (priority != value)
                {
                    int previous = priority;
                    priority = value;
                    if (signal != null)
                    {
                        signal.PriorityChanged(this, value, previous);
                    }
                }
            }
        }
    }

    public sealed class Slot<T1>:Slot
    {
        internal Slot()
        {

        }

        public new Signal<T1> Signal
        {
            get
            {
                return (Signal<T1>)signal;
            }
        }

        public new Action<T1> Listener
        {
            get
            {
                return (Action<T1>)listener;
            }
        }
    }

    public sealed class Slot<T1, T2>:Slot
    {
        internal Slot()
        {

        }

        public new Signal<T1, T2> Signal
        {
            get
            {
                return (Signal<T1, T2>)signal;
            }
        }

        public new Action<T1, T2> Listener
        {
            get
            {
                return (Action<T1, T2>)listener;
            }
        }
    }

    public sealed class Slot<T1, T2, T3>:Slot
    {
        internal Slot()
        {

        }

        public new Signal<T1, T2, T3> Signal
        {
            get
            {
                return (Signal<T1, T2, T3>)signal;
            }
        }

        public new Action<T1, T2, T3> Listener
        {
            get
            {
                return (Action<T1, T2, T3>)listener;
            }
        }
    }

    public sealed class Slot<T1, T2, T3, T4>:Slot
    {
        internal Slot()
        {

        }

        public new Signal<T1, T2, T3, T4> Signal
        {
            get
            {
                return (Signal<T1, T2, T3, T4>)signal;
            }
        }

        public new Action<T1, T2, T3, T4> Listener
        {
            get
            {
                return (Action<T1, T2, T3, T4>)listener;
            }
        }
    }
}